<?php
namespace Composer\Skyinstallers;

class ZendInstaller extends BaseInstaller
{
    protected $locations = array(
        'library' => 'library/{$name}/',
        'extra'   => 'extras/library/{$name}/',
        'module'  => 'module/{$name}/',
    );
}
