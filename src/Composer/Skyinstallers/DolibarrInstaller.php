<?php
namespace Composer\Skyinstallers;

/**
 * Class DolibarrInstaller
 *
 * @package Composer\Skyinstallers
 * @author  Raphaël Doursenaud <rdoursenaud@gpcsolutions.fr>
 */
class DolibarrInstaller extends BaseInstaller
{
    //TODO: Add support for scripts and themes
    protected $locations = array(
        'module' => 'htdocs/custom/{$name}/',
    );
}
