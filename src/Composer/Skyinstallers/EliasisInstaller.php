<?php
namespace Composer\Skyinstallers;

class EliasisInstaller extends BaseInstaller
{
    protected $locations = array(
        'module'  => 'modules/{$name}/'
    );
}
