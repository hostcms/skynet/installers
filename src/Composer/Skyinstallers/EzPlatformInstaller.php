<?php
namespace Composer\Skyinstallers;

class EzPlatformInstaller extends BaseInstaller
{
    protected $locations = array(
        'meta-assets' => 'web/assets/ezplatform/',
        'assets' => 'web/assets/ezplatform/{$name}/',
    );
}
