<?php
namespace Composer\Skyinstallers;

class PhiftyInstaller extends BaseInstaller
{
    protected $locations = array(
        'bundle' => 'bundles/{$name}/',
        'library' => 'libraries/{$name}/',
        'framework' => 'frameworks/{$name}/',
    );
}
