<?php
namespace Composer\Skyinstallers;

class HostCMSInstaller extends BaseInstaller
{
    protected $locations = array(
        'module'    => 'modules/{$vendor}{$name}/',
        'admin'     => 'admin/{$vendor}{$name}/',
        'skin'      => 'modules/skin/bootstrap/module/{$vendor}{$name}/',
    );


	/**
	 * {@inheritdoc}
	 */
	public function inflectPackageVars($vars)
	{
		if (isset($vars['name'])) {
			$name = preg_replace('/-(module|admin|skin)$/ui', '', $vars['name']);
			$vars['name'] = $name;
		}

		return parent::inflectPackageVars($vars);
	}
}
