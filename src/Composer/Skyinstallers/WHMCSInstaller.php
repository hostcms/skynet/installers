<?php

namespace Composer\Skyinstallers;

class WHMCSInstaller extends BaseInstaller
{
    protected $locations = array(
        'gateway' => 'modules/gateways/{$name}/',
    );
}
